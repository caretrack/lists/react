import * as React from "react";
import Table, { IColumns } from "./common/table";
import toastr from "toastr";
import axios from "axios";
import config from "../config.json";
import { IDateRange } from "./pages/Dashboard";

export interface IPATableProps {
  dateRange: IDateRange;
}

export interface IPATableState {
  isLoading?: boolean;
  items: Array<IRow>;
  totals: Array<TotalItem>;
  produced: Array<TotalItem>;
}

export interface IRow {
  [key: string]: Array<IITem>;
}

export interface IITem {
  id: string;
  total?: number;
  total_extended?: {
    current: number;
    new: number;
    missing: number;
  };
  period: { start: string; end: string };
  content?: Function;
  csvId?: string;
  patients?: Array<IPatients>;
  data: {
    rep: string;
    date: string;
    status: string;
  };
  isLoading?: boolean;
}

export interface TotalItem {
  id: string;
  total?: number;
  period: { start: string; end: string };
  total_extended?: {
    [key: string]: string;
  };
}

export interface IPatients {
  status: string;
  ProviderLName: string;
  ProviderNpi: string;
  IPAName: string;
  SubscriberNumber: string;
  MemberLName: string;
  MemberFName: string;
  DateOfBirth: string;
  MemberPhone: string;
  Gender: string;
  MemberEffectiveDate: string;
  MemberAddress: string;
  City: string;
  State: string;
  Zip: string;
  County: string;
  Language: string;
}

export const months = [
  { name: "Jan", end: 31 },
  { name: "Feb", end: 28 },
  { name: "Mar", end: 31 },
  { name: "Apr", end: 30 },
  { name: "May", end: 31 },
  { name: "Jun", end: 30 },
  { name: "Jul", end: 31 },
  { name: "Aug", end: 31 },
  { name: "Sept", end: 30 },
  { name: "Oct", end: 31 },
  { name: "Nov", end: 30 },
  { name: "Dec", end: 31 }
];

export const itemHasDate = (date: string, items: Array<any>) => {
  let indexOf = -1;
  for (let i = 0; i < items.length; i++) {
    if (items[i].period.start === date) indexOf = i;
  }
  return indexOf;
};

export const createEmptyArrayOfItems = (
  columns: Array<IColumns>,
  ipas: Array<string>
) => {
  let newItems: Array<IITem> = [];
  let newRow: IRow;
  let newData: Array<IRow> = [];

  // If data has no items inside go through IPAs
  ipas.map(ipa => {
    // For every column we create and push an empty item
    for (let i = 0; i < columns.length; i++) {
      // Map thorugh columns
      if (columns[i].period.start !== "") {
        // Empty item to push
        const emptyItem = {
          id: `tmp_${i}_${ipa}`,
          period: {
            start: columns[i].period.start,
            end: columns[i].period.end
          },
          data: { rep: "", date: "", status: "" }
        };
        // Push Empty Item into array
        newItems.push(emptyItem);
      }
      newRow = { [ipa]: newItems };
    }
    // data.push({ [ipa]: newItems });
    newData.push(newRow);
    newItems = [];
    return null;
  });
  // console.log(newData);
  return newData;
};

export const parseArrayWithLists = (lists: Array<IRow>, empty: Array<IRow>) => {
  if (lists.length > 0) {
    Object.keys(lists[0]).map((key, index) => {
      return lists[0][key].forEach(element => {
        const listItem = element;

        empty.forEach(emptyItem => {
          return Object.keys(emptyItem).map((ipa, ipaIndex) => {
            if (key === ipa) {
              const emptyRow = emptyItem[ipa];
              // console.log(emptyRow, ipa);

              for (let i = 0; i < emptyRow.length; i++) {
                if (emptyRow[i].period.start === listItem.period.start)
                  emptyItem[ipa][i] = listItem;
              }
            }
            return true;
          });
        });
      });
    });
  }

  return empty;
};

export const createFullTotalArray = (
  columns: Array<IColumns>,
  data: Array<TotalItem>
) => {
  const newItems: any = Array<TotalItem>();
  for (let i = 0; i < columns.length; i++) {
    // Check that is not the first column
    if (columns[i].period.start !== "") {
      const emptyItem: TotalItem = {
        id: `tmp_${i}`,
        period: {
          start: columns[i].period.start,
          end: columns[i].period.end
        }
      };
      // Search every column for the IItem that matches its date
      const _index = itemHasDate(columns[i].period.start, data);
      if (_index >= 0) newItems.push(data[_index]);
      else newItems.push(emptyItem);
    }
  }
  return newItems;
};

class IPATable extends React.Component<IPATableProps, IPATableState> {
  importMembers = async () => {
    const { items } = this.state;
    const promises: Array<Promise<any>> = [];
    items.map(rows => {
      return Object.keys(rows).map((key, index) => {
        return rows[key].map(item => {
          try {
            if (!isNaN(parseInt(item.id))) {
              const result = new Promise(resolve => {
                axios.post(`${config.url}/lists/importPatients`, {
                  list: { id: item.id },
                  user: { token: localStorage.getItem("user.token") || "" }
                });
              });

              promises.push(result);
              // console.log(result);
              return result;
            }
            return null;
          } catch (ex) {
            // console.log(ex.message);
            return ex;
          }
        });
      });
    });
    try {
      Promise.all(promises).then(results => {
        console.info("done", results);
      });
    } catch (ex) {}
  };

  componentDidUpdate(prevProps: IPATableProps) {
    if (prevProps.dateRange.start !== this.props.dateRange.start)
      this.getLists(this.props.dateRange, this.props.dateRange.month);
  }

  getLists = async (dateRange?: IDateRange, month?: number) => {
    try {
      let postObj;
      typeof dateRange === "undefined"
        ? (postObj = {
            user: {
              token: localStorage.getItem("user.token"),
              period: { start: "", end: "" }
            }
          })
        : (postObj = {
            user: { token: localStorage.getItem("user.token") },
            period: {
              start: dateRange.startRequest,
              end: dateRange.endRequest
            }
          });

      this.setState({ isLoading: true });
      // Get Data from API
      const {
        data: {
          response: {
            data: { totals, lists, produced, ipas }
          }
        }
      } = await axios.post(`${config.url}/lists/getLists`, postObj);
      this.setState({ isLoading: false });

      // Create columns based on the current month
      const createdColumns = month
        ? this.createColumns(month)
        : this.createColumns();

      let data = createEmptyArrayOfItems(createdColumns, ipas);
      data = parseArrayWithLists(lists, data);
      const newTotals = createFullTotalArray(createdColumns, totals);
      const newProduced = createFullTotalArray(createdColumns, produced);
      this.columns = createdColumns;

      this.setState({ totals: newTotals, items: data, produced: newProduced });
    } catch (ex) {
      // switch (true) {
      //   case ex.response.data.code >= 500:
      //     console.error(ex.response.data);
      //     toastr.error("A fatal error ocurred.");
      //     break;
      //   case ex.response.data.code >= 400:
      //     console.warn(ex.response.data);
      //     toastr.warning(ex.response.data.response.message);
      //     break;
      //   default:
      //     ex.response
      //       ? toastr.error(ex.response.data.response.message)
      //       : toastr.error(ex.message);
      //     break;
      console.log(ex.message);
      // }
      this.setState({ isLoading: false });
    }
  };

  createColumns = (_month?: number) => {
    const { dateRange } = this.props;
    const columns: Array<IColumns> = [
      { path: "IPA", label: "IPA", period: { start: "", end: "" } }
    ];
    // var month: number;
    const startDate = new Date(dateRange.startRequest.replace(/-/g, "/"));

    // console.log(startDate);

    for (let i = 0; i < config.monthRange; i++) {
      const monthString = ("0" + (startDate.getMonth() + 1)).slice(-2);
      const column = {
        path: `${this.getLabel(
          `${startDate.getFullYear()}-${monthString}-01`
        )}`,
        label: this.getLabel(`${startDate.getFullYear()}-${monthString}-01`),
        period: {
          start: `${startDate.getFullYear()}-${monthString}-01`,
          end: `${startDate.getFullYear()}-${monthString}-${
            months[startDate.getMonth()].end
          }`
        }
      };
      columns.push(column);
      // console.log(startDate);
      startDate.setMonth(startDate.getMonth() + 1);
    }
    // console.log(columns);
    return columns;
  };

  getLabel = (start: string) => {
    // console.log(start);
    const date = start.split("-");
    let year = parseInt(date[0]);
    let month = parseInt(date[1]);

    return `${months[month - 1].name},${year}`;
  };

  state: IPATableState = {
    items: [],
    totals: [],
    produced: []
  };

  columns: Array<IColumns> = [];

  render() {
    const { items, totals, produced } = this.state;

    return this.state.isLoading ? (
      <div className="d-flex justify-content-center bg-white p-5 ">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    ) : (
      <React.Fragment>
        <Table
          columns={this.columns}
          data={items}
          totals={totals}
          produced={produced}
        />
        <button
          className="membersBtn"
          style={{ float: "right", background: "white", display: "none" }}
          onClick={this.importMembers}
        >
          Import Members
        </button>
      </React.Fragment>
    );
  }
}

export default IPATable;
