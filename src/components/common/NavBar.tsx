import React from "react";
import { Link, NavLink } from "react-router-dom";

interface NavBarProps {
  title: string;
  items: Array<NavBarItem>;
}

export interface NavBarItem {
  title: string;
  link: string;
}

const NavBar = ({ items, title }: NavBarProps) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light position-fixed w-100">
      <Link className="navbar-brand" to="/">
        {title}
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="true"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <div className="navbar-nav">
          {items.map(navItem => (
            <NavLink
              key={`${navItem.link}_${navItem.title}`}
              className="nav-item nav-link"
              to={navItem.link}
            >
              {navItem.title}
            </NavLink>
          ))}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
