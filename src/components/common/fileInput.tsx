import React from "react";

interface IFileInputProps {
  name: string;
  label: string;
  placeholder: string;
  handleFileChange: React.ChangeEventHandler;
}

export default function FileInput(props: IFileInputProps) {
  const { name, label, placeholder, handleFileChange } = props;
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <div className="custom-file">
        <input
          type="file"
          className="custom-file-input"
          name={name}
          id={name}
          onChange={handleFileChange}
        />
        <label className="custom-file-label" htmlFor="inputGroupFile01">
          {placeholder}
        </label>
      </div>
    </div>
  );
}
