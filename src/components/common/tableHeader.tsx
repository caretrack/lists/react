import React from "react";
import { IColumns } from "./table";

interface ITableHeaderProps {
  columns: Array<IColumns>;
}

class TableHeader extends React.Component<ITableHeaderProps> {
  render() {
    return (
      <thead style={{ borderBottom: "1px solid black" }}>
        <tr>
          {this.props.columns.map(column => (
            <th className="clickable" key={column.path || column.key}>
              <b style={{ color: "#737373" }}>{column.label}</b>
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}

export default TableHeader;
