import React from "react";
import axios from "axios";
import config from "../../config.json";
import toastr from "toastr";
import { IPatients, IITem, IRow, TotalItem } from "../IPATable";
import { IColumns } from "./table";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Badge from "react-bootstrap/Badge";
import "../../recursos/css/tableBody.css";
import ListCell from "../ReusableComponents/listCell";
import UploadButton from "../ReusableComponents/UploadButton";
import TotalCell from "../ReusableComponents/TotalCell";
import PendingCell from "../ReusableComponents/PendingCell";
import SingleTotalCell from "../ReusableComponents/singleTotalCell";

interface ITableBodyProps {
    data: Array<IRow>;
    columns: Array<IColumns>;
    totals: Array<TotalItem>;
    produced: Array<TotalItem>;
}

interface ITableBodyState {
    isLoading?: boolean;
    data: Array<IRow>;
    show: boolean;
    selectedItem?: IITem;
}

class TableBody extends React.Component<ITableBodyProps, ITableBodyState> {
    state: ITableBodyState = {
        data: [],
        show: false,
        selectedItem: undefined,
    };

    handleFileChange = async (
        { target: input }: React.ChangeEvent<HTMLInputElement>,
        item: IITem,
        ipa: string,
    ) => {
        // console.log("handling files..", item);
        if (input.files !== null) {
            const file = input.files[0];
            const data = new FormData();
            let ipaFormatted = item.id.split("_")[2];
            ipaFormatted = ipa.toLowerCase().replace(" ", "");

            data.append("list", file);
            data.append("user[token]", localStorage.getItem("user.token") || "");
            data.append("ipa", ipaFormatted);
            data.append("period[start]", item.period.start);
            data.append("period[end]", item.period.end);

            const headers = {
                ContentType: "false",
                processData: "false",
                dataType: "json",
            };

            try {
                this.setState({ isLoading: true });
                item.isLoading = true;
                const {
                    data: {
                        message,
                        data: result,
                    },
                } = await axios.post(`${config.url}/lists/uploadFile`, data, {
                    headers: headers,
                });

                toastr.success(message);
                item.isLoading = false;
                this.setState({ isLoading: false });

                item.id = result.list.id;
                item.total = result.totals.count;
                item.patients = result.patients;
                item.total_extended = {
                    current: result.totals.count,
                    new: result.totals.new,
                    missing: result.totals.missing,
                };

                let newData;
                this.state.data.length > 0
                    ? (newData = this.state.data)
                    : (newData = this.props.data);

                this.setState({ data: newData });
            } catch (ex) {
                item.isLoading = false;
                this.setState({ isLoading: false });
                // console.log(ex);
                if (ex.response) {
                    const {
                        data: {
                            code,
                            response: { message },
                        },
                    } = ex.response;
                    switch (code) {
                        case 400:
                            toastr.warning(message);
                            break;
                        case 500:
                            toastr.error(message);
                            break;
                    }
                } else {
                    toastr.error(ex.message);
                }
            }
        }
    };

    renderUploadButton = (item: IITem, ipa: string) => {
        return (this.state.isLoading && item.isLoading ? (
                    <div className="d-flex justify-content-center bg-white p-5 "
                         style={{ color: 'red' }}>
                        <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>) :
                <UploadButton
                    itemId={item.id}
                    handleFileChange={e => this.handleFileChange(e, item, ipa)}
                />
        );
    };

    handleModalShow = (
        e: React.MouseEvent<HTMLTableCellElement>,
        item: IITem,
    ) => {
        this.setState({ selectedItem: item, show: true });
    };

    renderCell = (
        item: IITem,
        ipa: string,
        key: string,
        index: number,
        column: IColumns,
    ) => {
        // console.log(item);
        if (item === undefined)
            item = {
                id: `${key}${ipa}`,
                period: { start: column.period.start, end: column.period.end },
                data: {
                    rep: "",
                    date: "",
                    status: "",
                },
            };

        let style = {};
        if (item.total_extended) {
            item.total_extended.new < 0
                ? (style = { backgroundColor: "#F6CACF", color: "#6E3637" })
                : (style = { backgroundColor: "#BCCCE1", color: "black" });
        }

        return (item.total && item.total_extended ? (
            <td
                key={this.createRowKey(key || "", index)}
                style={{ padding: 5, alignItems: "center", minWidth: 228, maxWidth: 228 }}
            >
                <ListCell
                    list={item}
                    handleReset={e => this.handleFileChange(e, item, ipa)}
                />
            </td>
        ) : (<td
                key={this.createRowKey(key || "", index)}
                style={{ padding: 0, alignItems: "center", alignContent: "center" }}
            >
                {this.renderUploadButton(item, ipa)}
            </td>
        ));
    };

    renderTotalCell = (item: TotalItem, cell?: string) => {
        return item.total_extended ? (
            <td key={item.id}>
                <TotalCell
                    _total={item.total ? item.total : 0}
                    _current={item.total_extended ? item.total_extended.current : ""}
                    _missing={item.total_extended ? item.total_extended.missing : ""}
                    _new={item.total_extended ? item.total_extended.new : ""}
                    _produced={item.total ? item.total : 0}
                />
            </td>
        ) : (
            <td key={item.id}>
                <PendingCell />
            </td>
        );
    };

    createKey = (item: IITem, column: IColumns) => {
        return item.id + (column.path || column.key);
    };

    createRowKey = (key: string, index: number) => {
        return `${key}${index}`;
    };

    render() {
        const { columns, totals, produced, data: propsData } = this.props;
        const { show, selectedItem, data: stateData } = this.state;
        let data: Array<IRow> = [];
        stateData.length > 0 ? (data = stateData) : (data = propsData);

        let patients: Array<IPatients>;
        if (selectedItem)
            selectedItem.patients
                ? (patients = selectedItem.patients)
                : (patients = []);
        else patients = [];

        return (
            <>
                <tbody>
                <tr className="totals" style={{}}>
                    <td>
                        <strong>Totals</strong>
                    </td>
                    {columns.map(column => {
                        return totals.map(cell => {
                            return cell.period.start === column.period.start
                                ? this.renderTotalCell(cell, "current")
                                : null;
                        });
                    })}
                </tr>
                {data.map(item =>
                    Object.keys(item).map((key, index) => (
                        //item[key] has the array of IITems which you have to iterate
                        <tr key={this.createRowKey(key, index)}>
                            <td key={`${index}${key}`}>
                                <b>{key}</b>
                            </td>
                            {columns.map((iKey, iIndex) => {
                                return iIndex < columns.length - 1
                                    ? this.renderCell(
                                        item[key][iIndex],
                                        key,
                                        iKey.path || "",
                                        iIndex,
                                        columns[iIndex + 1],
                                    )
                                    : null;
                            })}
                        </tr>
                    )),
                )}
                </tbody>

                <Modal show={show} onHide={() => this.setState({ show: false })}>
                    <Modal.Header closeButton>
                        <Modal.Title>Imported Patients</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        {patients.map(patient => (
                            <React.Fragment
                                key={`${patient.MemberFName}${patient.MemberLName}`}
                            >
                                <Button variant="primary">
                                    {`${patient.MemberFName} ${patient.MemberLName} - ${patient.MemberAddress}`}
                                    <Badge variant="light">{patient.status}</Badge>
                                    <span className="sr-only">unread messages</span>
                                </Button>
                                <br />
                            </React.Fragment>
                        ))}
                    </Modal.Body>

                    <Modal.Footer></Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default TableBody;
