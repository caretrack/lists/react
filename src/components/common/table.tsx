import React from "react";
import TableHeader from "./tableHeader";
import TableBody from "./tableBody";
import { TotalItem, IRow } from "../IPATable";

interface ITableProps {
  columns: Array<IColumns>;
  data: Array<IRow>;
  totals: Array<TotalItem>;
  produced: Array<TotalItem>;
}

export interface IColumns {
  path?: string;
  key?: string;
  label?: string;
  period: { start: string; end: string };
}

const Table: React.FunctionComponent<ITableProps> = ({
  columns,
  data,
  totals,
  produced
}) => {
  return (
    <table
      className="table"
      style={{ border: "1px solid #EBEBEB", textAlign: "center" }}
    >
      <TableHeader columns={columns} />
      <TableBody
        data={data}
        columns={columns}
        totals={totals}
        produced={produced}
      />
    </table>
  );
};

export default Table;
