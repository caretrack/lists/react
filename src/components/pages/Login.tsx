import * as React from "react";
import Logo from "../../recursos/img/logo.png";
import axios from "axios";
import toastr from "toastr";
import "../../recursos/css/app.scss";
import config from "../../config.json";

interface ILoginComponentState {
  user: { token?: string };
  logged: boolean;
  account: {
    login: string;
    password: string;
    [key: string]: string;
  };
}

export default class Login extends React.Component<any, ILoginComponentState> {
  state: ILoginComponentState = {
    user: { token: "" },
    logged: false,
    account: {
      login: "",
      password: ""
    }
  };

  handleChange: React.ChangeEventHandler<HTMLInputElement> = ({
    target: input
  }) => {
    const account = { ...this.state.account };

    account[input.name] = input.value;
    this.setState({ account });
  };

  private iniciarSesion: React.FormEventHandler<HTMLFormElement> = async e => {
    e.preventDefault();
    const data = { ...this.state.account };

    try {
      const { data: result } = await axios.post(
        `${config.url}/login/iniciarSesion`,
        data
      );
      this.props.location.state = {
        user: { token: result.response.data.token }
      };
      this.setState({
        user: { token: result.response.data.token },
        logged: true
      });
      localStorage.setItem("user.token", result.response.data.token);
      this.props.history.push({
        pathname: "/Dashboard",
        state: { user: { token: result.response.data.token }, logged: true }
      });
    } catch (ex) {
      toastr.error(ex.message, "Error");
    }
  };

  render() {
    const { login, password } = this.state.account;

    return (
      <React.Fragment>
        <form onSubmit={this.iniciarSesion} className="form-signin">
          <div className="logo">
            <div className="pull-center">
              <div className="navbar-brand">
                <img src={Logo} alt="." />
              </div>
            </div>
          </div>
          <h3
            className="h3 mb-3 font-weight-normal"
            style={{ textAlign: "center" }}
          >
            IMPORT LISTS
          </h3>
          <div className="box-color r text-color container">
            <div className="form-group">
              <label>User</label>
              <input
                name="login"
                type="text"
                className="form-control"
                required
                autoComplete="off"
                value={login}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                name="password"
                type="password"
                className="form-control"
                required
                autoComplete="off"
                value={password}
                onChange={this.handleChange}
              />
            </div>
            <button
              type="submit"
              className="btn btn-lg btn-raised btn-primary btn-block"
            >
              Login
            </button>
          </div>
        </form>
      </React.Fragment>
    );
  }
}
