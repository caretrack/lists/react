import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import IPATable from "../IPATable";

import config from "../../config.json";
import "../../recursos/css/dashboard.css";
import "../../recursos/css/listCell.css";
import NavBar, { NavBarItem } from "../common/NavBar";

interface DashboardProps extends RouteComponentProps<any> {}

interface DashboardState {
  user: {
    id: string;
    nombre: string;
    login: string;
    correo: string;
    perfil: string;
    token: string;
    logged: string;
  };
  fileInput: Array<File>;
  fileName?: string;
  dateRange: IDateRange;
  isIncreasable: boolean;
}

export interface IDateRange {
  start: string;
  end: string;
  startRequest: string;
  endRequest: string;
  month: number;
}

class Dashboard extends React.Component<DashboardProps, DashboardState> {
  state = {
    user: {
      id: "",
      nombre: "",
      login: "",
      correo: "",
      perfil: "",
      token: "",
      logged: ""
    },
    fileInput: [],
    fileName: "",
    dateRange: {
      start: "",
      end: "",
      startRequest: "",
      endRequest: "",
      month: 0
    },
    isIncreasable: false
  };

  componentWillMount() {
    document.addEventListener("keydown", event => {
      if (event.keyCode === 37) this.handleBackMonths();
      else if (event.keyCode === 39 && this.state.isIncreasable)
        this.handleForwardMonths();
    });
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", ev => {});
  }

  componentDidMount = () => {
    const _month = new Date().getMonth() + 1;
    const dateRange = this.createDateRange(_month);
      this.setState({
      // @ts-ignore
      user: this.props.location.state,
      dateRange
    });
  };

  handleBackMonths = () => {
    let { dateRange } = this.state;
    let stringMonth: IDateRange = {
      start: "",
      end: "",
      startRequest: "",
      endRequest: "",
      month: 0
    };

    let startingDate = new Date(dateRange.startRequest.replace(/-/g, "/"));
    let endingDate = new Date(dateRange.endRequest.replace(/-/g, "/"));

    // console.log(startingDate, endingDate);

    startingDate = new Date(
      startingDate.getFullYear(),
      startingDate.getMonth() - 1,
      1
    );
    endingDate = new Date(endingDate.getFullYear(), endingDate.getMonth(), 0);
    // console.log(startingDate, endingDate);
    stringMonth.end = config.months[endingDate.getMonth()].name;
    stringMonth.start = config.months[startingDate.getMonth()].name;
    stringMonth.month = dateRange.month - 1;
    stringMonth.startRequest = `${startingDate.getFullYear()}-${(
      "0" +
      (startingDate.getMonth() + 1)
    ).slice(-2)}-01`;
    stringMonth.endRequest = `${endingDate.getFullYear()}-${(
      "0" +
      (endingDate.getMonth() + 1)
    ).slice(-2)}-${config.months[endingDate.getMonth()].days}`;
    // console.log(stringMonth);
    this.setState({ dateRange: stringMonth, isIncreasable: true });
  };

  handleForwardMonths = () => {
    let { dateRange } = this.state;
    let stringMonth: IDateRange = {
      start: "",
      end: "",
      startRequest: "",
      endRequest: "",
      month: 0
    };

    let startingDate = new Date(dateRange.startRequest.replace(/-/g, "/"));
    let endingDate = new Date(dateRange.endRequest.replace(/-/g, "/"));

    // console.log(startingDate, endingDate);

    startingDate = new Date(
      startingDate.getFullYear(),
      startingDate.getMonth() + 1,
      1
    );
    endingDate = new Date(
      endingDate.getFullYear(),
      endingDate.getMonth() + 2,
      0
    );
    // console.log(startingDate, endingDate);
    stringMonth.end = config.months[endingDate.getMonth()].name;
    stringMonth.start = config.months[startingDate.getMonth()].name;
    stringMonth.month = dateRange.month++;
    stringMonth.startRequest = `${startingDate.getFullYear()}-${(
      "0" +
      (startingDate.getMonth() + 1)
    ).slice(-2)}-01`;
    stringMonth.endRequest = `${endingDate.getFullYear()}-${(
      "0" +
      (endingDate.getMonth() + 1)
    ).slice(-2)}-${config.months[endingDate.getMonth()].days}`;
    // console.log(stringMonth);

    this.isThisMonth(stringMonth.endRequest)
      ? this.setState({ dateRange: stringMonth, isIncreasable: false })
      : this.setState({ dateRange: stringMonth });
  };

  isThisMonth = (date: string) => {
    const dateArray = date.split("-");
    const dateYear = dateArray[0];
    const dateMonth = dateArray[1];
    if (
      dateYear === new Date().getFullYear().toString() &&
      dateMonth === (new Date().getMonth() + 1).toString()
    )
      return true;

    return false;
  };

  createDateRange = (month: number, wasIncreased?: boolean) => {
    const { dateRange } = this.state;
    const value = config.monthRange;

    const today = new Date();
    let startingDate: Date;
    let endingDate: Date;
    let stringMonth: IDateRange = {
      start: "",
      end: "",
      startRequest: "",
      endRequest: "",
      month
    };

    dateRange.startRequest === ""
      ? (startingDate = new Date(today.setMonth(today.getMonth() + 1 - value)))
      : (startingDate = new Date(dateRange.startRequest));
    dateRange.endRequest === ""
      ? (endingDate = new Date())
      : (endingDate = new Date(dateRange.endRequest));

    // console.log(endingDate);

    const startMonth = ("0" + (startingDate.getMonth() + 1)).slice(-2);
    const endMonth = ("0" + (endingDate.getMonth() + 1)).slice(-2);
    // console.log(endMonth);

    for (let i = 0; i < config.months.length; i++) {
      if (config.months[i].id === startMonth) {
        stringMonth.start = config.months[i].name;
        stringMonth.startRequest = `${startingDate.getFullYear()}-${startMonth}-01`;
      } else if (config.months[i].id === endMonth) {
        stringMonth.end = config.months[i].name;
        stringMonth.endRequest = `${endingDate.getFullYear()}-${endMonth}-${
          config.months[i].days
        }`;
      }
    }

    return stringMonth;
  };

  render() {
    const { dateRange, isIncreasable } = this.state;

    const items: Array<NavBarItem> = [
      { title: "IPA Roaster Config", link: "" },
      { title: "IPA Roaster Alias", link: "IPA Roaster Alias" },
      { title: "HMO Integrator", link: "" }
    ];

    return (
      <React.Fragment>
        <NavBar title="Caretrack" items={items} />
        <div style={{ display: "block", float: "left" }}>
          {/* <button className="membersBtn mt-5" onClick={this.handleBackMonths}>
            Go Back
          </button>
          <button
            className={isIncreasable ? "membersBtn" : "dissapear"}
            onClick={this.handleForwardMonths}
          >
            Go Forward
          </button> */}
        </div>
        <div className="p-2 pt-5 ">
          <IPATable dateRange={dateRange} />
        </div>
      </React.Fragment>
    );
  }
}

export default Dashboard;
