import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFirstAid } from "@fortawesome/free-solid-svg-icons";

export interface SingleTotalCellProps {
  total: number;
}

export default function SingleTotalCell(props: SingleTotalCellProps) {
  const { total } = props;
  // Define dynamic styles
  let background = "#fff";
  let cellColor = "#000";
  let message = "in Total";
  // Meet conditions
  if (total < 0) {
    background = "#AE3251";
    cellColor = "#fff";
    message = "Please review!";
  } else if (total > 0) {
    background = "#51BFBD";
    cellColor = "#fff";
    message = "Congrats!";
  }

  return (
    <div
      className="box text-left p-4 mx-auto border"
      style={{
        borderRadius: 10,
        backgroundColor: background,
        border: "1px solid #EFEFEF",
        color: cellColor,
        width: 228
      }}
    >
      <div className="box-header primary container mx-auto">
        <div className="row mx-auto">
          <FontAwesomeIcon icon={faFirstAid} size="2x" className="ml-2 mr-3" />
          <h3>{total}</h3>
          <span
            className="m-2 font-weight-bold"
            style={{ fontSize: 13, textAlign: "center" }}
          >
            {message}
          </span>
        </div>
      </div>
    </div>
  );
}
