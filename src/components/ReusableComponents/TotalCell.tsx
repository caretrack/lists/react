import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGlassCheers,
  faFrown,
  faBacon,
  faStar,
  faPercentage
} from "@fortawesome/free-solid-svg-icons";

export interface TotalCellProps {
  _total: number;
  _produced: number;
  _new: string;
  _current: string;
  _missing: string;
}

export default function TotalCell(props: TotalCellProps) {
  const { _new, _current, _missing, _total, _produced } = props;
  return (
    <div
      className="container pt-0 border"
      style={{ ...styles.container, backgroundColor: "#ED4B56" }}
    >
      <div className="row p-2 text-white">
        <div className="col-7 text-left">
          <div className="row p-1 m-0">
            <FontAwesomeIcon icon={faStar} color="#fff" className="mr-2" />
            <h2 style={{ color: "#fff", fontSize: 16 }}>Total</h2>
          </div>
          <div className="row p-1">
            <span
              style={{ fontSize: 11, color: "#efefef", fontWeight: "bold" }}
            >
              Summary
            </span>
          </div>
        </div>
        <div className="col-5">
          <div
            style={{ fontSize: 20, textAlign: "left" }}
            className=" p-1 font-weight-bold text-white"
          >
            {_total}
          </div>
        </div>
      </div>
      <div className="row p-2 bg-white">
        <div className="col-8 text-left">
          <div className="row p-1">
            <FontAwesomeIcon
              icon={faGlassCheers}
              color="#a0a0a0"
              className="mr-2"
            />
            <h2 style={styles.sectionTitle}>New</h2>
          </div>
          <div className="row p-1">
            <span style={{ ...styles.sectionContent, fontWeight: "bold" }}>
              Against last month
            </span>
          </div>
        </div>
        <div className="col-4">
          <div className="rounded-pill bg-secondary p-1 font-weight-bold text-white">
            {_new}
          </div>
        </div>
      </div>
      <div className="row border-top border-bottom p-2 bg-white">
        <div className="col-8 text-left">
          <div className="row p-1">
            <FontAwesomeIcon icon={faBacon} color="#a0a0a0" className="mr-2" />
            <h2 style={styles.sectionTitle}>Current</h2>
          </div>
          <div className="row p-1">
            <span style={{ ...styles.sectionContent, fontWeight: "bold" }}>
              Against last month
            </span>
          </div>
        </div>
        <div className="col-4">
          <div
            className="rounded-pill p-1 font-weight-bold  text-white"
            style={{ backgroundColor: "#25245E" }}
          >
            {_current}
          </div>
        </div>
      </div>
      <div className="row p-2 bg-white">
        <div className="col-8 text-left">
          <div className="row p-1">
            <FontAwesomeIcon icon={faFrown} color="#a0a0a0" className="mr-2" />
            <h2 style={styles.sectionTitle}>Missing</h2>
          </div>
          <div className="row p-1">
            <span style={{ ...styles.sectionContent, fontWeight: "bold" }}>
              Against last month
            </span>
          </div>
        </div>
        <div className="col-4">
          <div className="rounded-pill bg-danger p-1 font-weight-bold text-white">
            {_missing}
          </div>
        </div>
      </div>
      <div
        className="row  p-2 bg-white"
        style={{ borderTop: "2px solid #25245E" }}
      >
        <div className="col-8 text-left">
          <div className="row p-1">
            <FontAwesomeIcon
              icon={faPercentage}
              color="#a0a0a0"
              className="mr-2"
            />
            <h2 className="font-weight-bold" style={styles.sectionTitle}>
              Produced
            </h2>
          </div>
          <div className="row p-1">
            <span style={{ ...styles.sectionContent, fontWeight: "bold" }}>
              Against last month
            </span>
          </div>
        </div>
        <div className="col-4">
          <div
            className="rounded-pill p-1 font-weight-bold text-white"
            style={{ backgroundColor: "#25245E" }}
          >
            {_produced}
          </div>
        </div>
      </div>
    </div>
  );
}

const styles = {
  container: {
    background: "#fff",
    borderRadius: "10px",
    width: 228
  },
  section: {
    padding: 8,
    color: "#a0a0a0",
    display: "block",
    border: "1px solid black"
  },
  sectionTitle: {
    fontSize: 15,
    color: "#505050",
    margin: "0"
  },
  sectionContent: { fontSize: 11, fontWeight: "bold", color: "#afafaf" }
};
