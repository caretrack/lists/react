import * as React from "react";
import axios from "axios";
import config from "../../config.json";
import toastr from "toastr";
import { IITem } from "../IPATable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faExchangeAlt,
  faUpload
} from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { useEffect } from "react";

export interface ListCellProps {
  list: IITem;
  handleReset: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export default function ListCell(props: ListCellProps) {
  const { list, handleReset } = props;

  const [isLoading, setIsLoading] = useState(false);
  const [status, setStatus] = useState(list.data.status);

  let statusColor = "#EA7377";

  async function importList(list: IITem) {
    setIsLoading(true);
    try {
      if (!isNaN(parseInt(list.id))) {
        const {
          data: { code }
        } = await axios.post(`${config.url}/lists/importPatients`, {
          list: { id: list.id },
          user: { token: localStorage.getItem("user.token") || "" }
        });
        if (code === 200) {
          setStatus("completed");
          setIsLoading(false);
          window.location.reload();
        } else toastr.error("There was an unexpected error");
      }
    } catch (ex) {
      toastr.error(ex.message);
      setIsLoading(false);
      return ex;
    }
  }

  switch (status) {
    case "completed":
      statusColor = "#25245E";
      break;
    case "pending":
      statusColor = "#EA7377";
      break;
  }

  useEffect(() => {});

  return isLoading ? (
    <div className="d-flex justify-content-center bg-white p-5 "
      style={styles.container}>
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  ) : (
    <div
      className={" container border position-relative"}
      style={{
        backgroundColor: statusColor,
        borderRadius: 10
      }}
    >
      <div className="p-2 row m-0">
        <div className="col-8">
          <span
            className="text-white m-0"
            style={{ ...styles.totalText, fontWeight: "bold" }}
          >
            {list.total}
          </span>
        </div>
        {/*<div className="col-2 text-white">*/}
        {/*  <FontAwesomeIcon icon={faExchangeAlt} color="#fff" />*/}
        {/*</div>*/}
        {/*<div className="col-4">*/}
        {/*  <span*/}
        {/*    className="text-white m-0"*/}
        {/*    style={{ ...styles.totalText, fontWeight: "bold" }}*/}
        {/*  >*/}
        {/*    {1890}*/}
        {/*  </span>*/}
        {/*</div>*/}
        <div className="col-4">
          <input
            type="file"
            className="inputfile"
            name={`${list.id}_input`}
            id={`${list.id}_input`}
            onChange={handleReset}
          />
          <label htmlFor={`${list.id}_input`} style={{ width: 35, height: 35 }}>
            <FontAwesomeIcon color="#bfbfbf" icon={faTimes} />
          </label>
        </div>
      </div>
      <div className="row bg-white">
        <div className="col-10">
          <div className="row p-2">
            <span className="font-weight-bold m-2" style={{ fontSize: 10 }}>
              Uploaded By: {list.data && list.data.rep ? list.data.rep : "..."}
            </span>

            <span className="font-weight-bold m-1" style={{ fontSize: 12 }}>
              <div>• Uploaded On: </div>{list.data && list.data.date ? list.data.date : "..."}
            </span>
          </div>
          <div className="border-top p-2 row">
            <div className="col-4">
              <h2 className="font-weight-bold" style={styles.sectionTitle}>
                New
              </h2>
              <span className="font-weight-bold" style={styles.sectionContent}>
                {list.total_extended ? list.total_extended.new : null}
              </span>
            </div>
            <div className="col-4">
              <h2 className="font-weight-bold" style={styles.sectionTitle}>
                Current
              </h2>
              <span className="font-weight-bold" style={styles.sectionContent}>
                {list.total_extended ? list.total_extended.current : null}
              </span>
            </div>
            <div className="col-4">
              <h2 className="font-weight-bold" style={styles.sectionTitle}>
                Missing
              </h2>
              <span className="font-weight-bold" style={styles.sectionContent}>
                {list.total_extended ? list.total_extended.missing : null}
              </span>
            </div>
          </div>
          {status === "completed" ? null : (
            <div className="row border-top p-2" style={{ color: "#24265B" }}>
              {" "}
              <div className="col-10"></div>
              <div className="col-2">
                <span
                  onClick={() => {
                    importList(list);
                  }}
                >
                  <FontAwesomeIcon icon={faUpload} />
                </span>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

const styles = {
  container: {
    borderRadius: 10
  },
  totalText: {
    fontSize: "20px",

    justifyContent: "center"
  },
  sectionTitle: { fontSize: 13, color: "#afafaf" },
  sectionContent: { fontSize: 13, color: "#afafaf" }
};
