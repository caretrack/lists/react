import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelopeOpenText } from "@fortawesome/free-solid-svg-icons";

export default function PendingCell() {
  return (
    <div
      className="box text-left p-2"
      style={{
        borderRadius: 10,
        backgroundColor: "#F9EBAE",
        border: "1px solid #9C884D",
        color: "#A08843",
        width: 228
      }}
    >
      <div className="box-header primary container">
        <div className="row">
          <FontAwesomeIcon
            icon={faEnvelopeOpenText}
            size="2x"
            className="ml-2 mr-3"
          />
          <h3>Pending</h3>
        </div>
        <small>We don't have enough info to present data</small>
      </div>
    </div>
  );
}
