import React, { ChangeEvent } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload } from "@fortawesome/free-solid-svg-icons";

export interface UploadButtonProps {
  itemId: string;
  handleFileChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

export default function UploadButton(props: UploadButtonProps) {
  const { itemId, handleFileChange } = props;
  return (
    <div className="container p-2">
      <input
        type="file"
        className="inputfile"
        name={itemId}
        id={itemId}
        onChange={handleFileChange}
      />
      <label
        htmlFor={itemId}
        className="bg-white border container"
        style={styles.container}
      >
        <div className="p-0 text-left row">
          <FontAwesomeIcon icon={faUpload} className="m-3" />

          <span
            className=" font-weight-bold mt-3 mb-3"
            style={{ ...styles.title }}
          >
            Upload ...
          </span>
        </div>
        <div className="p-3 text-left row">
          <span
            style={{
              ...styles.description,
              textAlign: "left",
              fontWeight: "bold"
            }}
          >
            Select a file to upload
          </span>
        </div>
      </label>
    </div>
  );
}
const styles = {
  container: {
    borderRadius: "10px",
    width: 228
  },
  title: {
    fontSize: 13
  },
  description: {
    fontSize: 12,
    color: "#B3B3B3"
  }
};
