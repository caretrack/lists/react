import React from "react";
import {
  Route,
  Switch,
  Redirect,
  RouteComponentProps,
  withRouter
} from "react-router-dom";
import axios from "axios";
import toastr from "toastr";
import config from "./config.json";
import Login from "./components/pages/Login";
import Dashboard from "./components/pages/Dashboard";

import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "./recursos/css/toastr.css";

interface IAppComponentState {
  logged: boolean;
  user: { token: string; [key: string]: string };
}

interface IAppComponentProps extends RouteComponentProps<any> {
  children?: any;
}

class App extends React.Component<IAppComponentProps, IAppComponentState> {
  state: IAppComponentState = {
    logged: false,
    user: {
      token: ""
    }
  };

  componentDidMount = async () => {
    const token = localStorage.getItem("user.token") || "";
    await this.validarUsuario(token).then(response => {
      if (response !== undefined) {
        this.props.history.push({
          pathname: "/Dashboard",
          state: { user: response.user }
        });
      }
    });
  };

  validarUsuario = async (token: string) => {
    try {
      const {
        data: {
          code,
          response: { data }
        }
      } = await axios.post(`${config.url}/login/validarUsuario`, {
        user: { token: token }
      });
      if (code === 200) {
        const user = { ...data.usuario, token };
        return { user, logged: true };
      }
    } catch (ex) {
      if (ex.response) {
        const {
          data: {
            code,
            response: { message }
          }
        } = ex.response;
        switch (code) {
          case 400:
            toastr.warning(message);
            this.props.history.push({
              pathname: "/"
            });
            break;
          case 500:
            toastr.error(message);
            break;
        }
        return undefined;
      } else {
        toastr.error(ex.message);
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <main style={{ padding: 0 }}>
          <Switch>
            <Route
              path="/login"
              render={() => {
                return <Login {...this.props} />;
              }}
            />
            <Route path="/Dashboard" component={Dashboard} />
            <Redirect from="/" exact to="/login" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}
export default withRouter(App);

export function isEmpty(obj: Object) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}
