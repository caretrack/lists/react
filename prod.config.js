const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: "./webpack/src/entry",
    output: {
        path: path.resolve(__dirname, "webpack/www/assets")
    },
    resolve: {
        extensions: ['.js', '.jsx', '.tsx']
    },
    plugins: [
        new ExtractTextPlugin({filename: 'bundle.css', allChunks: true}),
        new CopyPlugin([
            {from: 'img', to: '../img'},
            {from: 'images', to: 'images'},
        ]),
    ],
    module: {
        rules: [
            {
                test: /\.jsx/,
                use: {
                    loader: 'babel-loader',
                    options: {"presets": ["@babel/preset-env", "@babel/preset-react"]}
                }
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/, loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader'})
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader!sass-loader'})
            },
            {
                test: /\.png$/,
                loader: 'file-loader?name=images/[name].[ext]'
            },
        ]
    }
};